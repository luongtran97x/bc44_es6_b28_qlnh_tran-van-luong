const BASE_URL = "https://643a58b8bd3623f1b9b16433.mockapi.io/fodd";
import { Food } from "../../models/v1/model.js";
import { renderFoodList } from "./controller-v2.js";
import { layThongTinTuForm } from "../v1/controller.js";
let idSelected = null;
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let dataArr = res.data.map((item) => {
        let { name, type, discount, img, desc, price, status, id } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFoodList(dataArr);
    })
    .catch((err) => {});
};
fetchFoodList();

window.btnXoa = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
    })
    .catch((err) => {});
};
window.btnSua = (id) => {
  $("#exampleModal").modal("show");
  idSelected = id
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      // show thông tin lên form
      let { id, type, price, img, status, desc, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {});
};

window.themMon = () => {
  let data = layThongTinTuForm();
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {});
};
window.capNhat = (id) =>{

  let data = layThongTinTuForm();
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount:data.khuyenMai,
    img: data.hinhMon,
    price:data.giaMon,
    status:data.tinhTrang,
  }
   axios({
    url: `${BASE_URL}/${idSelected}`,
    method:"PUT",
    data: newFood,
   })
   .then((res)=>{
    $("#exampleModal").modal("hide");
    fetchFoodList()
   })
   .catch((err)=>{
    
   })
}
/**
 {
    "name": "Product Paradigm Architect",
    "type": true,
    "discount": 58983,
    "img": "https://loremflickr.com/640/480/food",
    "desc": "18874-6147",
    "price": 5598,
    "status": false,
    "id": "3"
}
 */
