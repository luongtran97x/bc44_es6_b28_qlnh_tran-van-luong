export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, giaMon, khuyenMai, tinhGiaKM, tinhTrang } = item;
    let contentTr = `
     <tr>
        <td>${maMon}</td>    
        <td>${tenMon}</td>    
        <td>${loai === true ? "Chay" : "Mặn"}</td>    
        <td>${giaMon}</td>    
        <td>${khuyenMai}%</td>    
        <td>${item.tinhGiaKM()}</td>    
        <td>${tinhTrang === true ? "Còn" :"Hết"  }</td>  
        <td><button class="btn btn-danger" onclick ="btnXoa(${maMon})">DELETE</button>
        <button class="btn btn-success" onclick ="btnSua(${maMon})">EDIT</button>
        </td>        
     </tr>
        `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
